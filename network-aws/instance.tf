resource "aws_vpc" "vpc" {
  cidr_block           = var.cidr
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = {
    Name = "${var.workspace_id}-private-network-vpc"
  }
}

resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "${var.workspace_id}-igw"
  }
}

resource "aws_subnet" "subnet" {
  vpc_id            = aws_vpc.vpc.id
  cidr_block        = var.cidr
  availability_zone = var.availability_zone

  tags = {
    Name = "${var.workspace_id}-subnet"
  }
}

resource "aws_route_table" "router" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = "${var.workspace_id}-router"
  }
}

resource "aws_route_table_association" "table_association" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = aws_route_table.router.id
}
