provider "aws" {
  region     = var.aws_region
  access_key = var.aws_access_key
  secret_key = var.aws_secret_key
  token      = var.aws_session_token
  dynamic "assume_role" {
    for_each = var.assume_role_arn != "" ? [1] : []
    content {
      role_arn     = var.assume_role_arn
      session_name = var.assume_role_session_name
    }
  }
  default_tags {
    tags = {
      application_type   = var.application_type
      cloud_type         = var.cloud_type
      co_id              = var.co_id
      resource_type      = var.resource_type
      subscription       = var.subscription
      subscription_group = var.subscription_group
      wallet_id          = var.wallet_id
      workspace_id       = var.workspace_id
    }
  }
}
