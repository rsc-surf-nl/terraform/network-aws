output "id" {
  value = aws_vpc.vpc.id
}

output "vpc_id" {
  value = aws_vpc.vpc.id
}

output "network_id" {
  value = aws_subnet.subnet.id
}

output "network_name" {
  value = aws_vpc.vpc.tags.Name
}

output "subnet_id" {
  value = aws_subnet.subnet.id
}

output "router_id" {
  value = aws_route_table.router.id
}

output "subnet_cidr" {
  value = aws_vpc.vpc.cidr_block
}

output "description" {
  value = var.description
}
